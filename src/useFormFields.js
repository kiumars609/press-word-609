import React, { useState } from 'react'

export default function useFormFields(init) {
    const [fields, setFields] = useState({ init })
    const handleFields = (e) => {
        const { target } = e
        setFields({
            ...fields,
            [target.name]: target.value
        })
    }

    return { fields, handleFields, setFields }
}

import React from 'react'
import MaterialTable from "material-table";
import { UseCustomDelete } from '../../../Actions/UseCustomDelete'

export default function DataTable({ refreshData = '', filtring = false, title, data, deleteAction = false, deleteRoute }) {

    const del = {
        icon: 'delete',
        tooltip: 'Delete Data',
        iconProps: { color: 'error' },
        onClick: (event, data) => {
            UseCustomDelete({
                route: deleteRoute,
                id: data.id
            })
        }
    }
    return (
        <div style={{ maxWidth: '100%' }}>
            <MaterialTable
                options={{ filtering: filtring, actionsColumnIndex: -1 }}
                columns={title}
                data={data}
                actions={deleteAction && [del]}
                title={<a className="btn btn-success text-white m-2" onClick={refreshData} data-click="panel-reload"><i className="fa fa-redo"></i> Refresh</a>}
            />
        </div>
    )
}
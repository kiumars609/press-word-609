import React from 'react'

export default function ThemePanel() {
    return (
        <>
            <div className="theme-panel theme-panel-lg">
                <a href={void(0)} data-click="theme-panel-expand" className="theme-collapse-btn"><i className="fa fa-cog"></i></a>
                <div className="theme-panel-content">
                    <h5>App Settings</h5><ul className="theme-list clearfix">
                        <li><a href={void(0)} className="bg-red" data-theme="red" data-theme-file="../assets/css/default/theme/red.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-pink" data-theme="pink" data-theme-file="../assets/css/default/theme/pink.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-orange" data-theme="orange" data-theme-file="../assets/css/default/theme/orange.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-yellow" data-theme="yellow" data-theme-file="../assets/css/default/theme/yellow.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-lime" data-theme="lime" data-theme-file="../assets/css/default/theme/lime.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-green" data-theme="green" data-theme-file="../assets/css/default/theme/green.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green">&nbsp;</a></li>
                        <li className="active"><a href={void(0)} className="bg-teal" data-theme="default" data-theme-file="" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-aqua" data-theme="aqua" data-theme-file="../assets/css/default/theme/aqua.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-blue" data-theme="blue" data-theme-file="../assets/css/default/theme/blue.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-purple" data-theme="purple" data-theme-file="../assets/css/default/theme/purple.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-indigo" data-theme="indigo" data-theme-file="../assets/css/default/theme/indigo.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo">&nbsp;</a></li>
                        <li><a href={void(0)} className="bg-black" data-theme="black" data-theme-file="../assets/css/default/theme/black.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">&nbsp;</a></li>
                    </ul>
                    <div className="divider"></div>
                    <div className="row m-t-10">
                        <div className="col-6 control-label text-inverse f-w-600">Header Fixed</div>
                        <div className="col-6 d-flex">
                            <div className="custom-control custom-switch ml-auto">
                                <input type="checkbox" className="custom-control-input" name="header-fixed" id="headerFixed" value="1" defaultChecked />
                                <label className="custom-control-label" htmlFor="headerFixed">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <div className="row m-t-10">
                        <div className="col-6 control-label text-inverse f-w-600">Header Inverse</div>
                        <div className="col-6 d-flex">
                            <div className="custom-control custom-switch ml-auto">
                                <input type="checkbox" className="custom-control-input" name="header-inverse" id="headerInverse" value="1" />
                                <label className="custom-control-label" htmlFor="headerInverse">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <div className="row m-t-10">
                        <div className="col-6 control-label text-inverse f-w-600">Sidebar Fixed</div>
                        <div className="col-6 d-flex">
                            <div className="custom-control custom-switch ml-auto">
                                <input type="checkbox" className="custom-control-input" name="sidebar-fixed" id="sidebarFixed" value="1" defaultChecked />
                                <label className="custom-control-label" htmlFor="sidebarFixed">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <div className="row m-t-10">
                        <div className="col-6 control-label text-inverse f-w-600">Sidebar Grid</div>
                        <div className="col-6 d-flex">
                            <div className="custom-control custom-switch ml-auto">
                                <input type="checkbox" className="custom-control-input" name="sidebar-grid" id="sidebarGrid" value="1" />
                                <label className="custom-control-label" htmlFor="sidebarGrid">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <div className="row m-t-10">
                        <div className="col-md-6 control-label text-inverse f-w-600">Sidebar Gradient</div>
                        <div className="col-md-6 d-flex">
                            <div className="custom-control custom-switch ml-auto">
                                <input type="checkbox" className="custom-control-input" name="sidebar-gradient" id="sidebarGradient" value="1" />
                                <label className="custom-control-label" htmlFor="sidebarGradient">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                    <div className="divider"></div>
                    <h5>Admin Design (5)</h5>
                    <div className="theme-version">
                        <a href="../template_html/index_v2.html" className="active">
                            {/* <span style={{ backgroundImage: './assets/dashboard/img/theme/default.jpg' }}></span> */}
                        </a>
                        <a href="../template_transparent/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/theme/transparent.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../template_apple/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/theme/apple.jpg);"></span> */}
                        </a>
                        <a href="../template_material/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/theme/material.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../template_facebook/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/theme/facebook.jpg);"></span> */}
                        </a>
                        <a href="../template_google/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/theme/google.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="divider"></div>
                    <h5>Language Version (7)</h5>
                    <div className="theme-version">
                        <a href="../template_html/index_v2.html" className="active">
                            {/* <span style="background-image: url(../assets/img/version/html.jpg);"></span> */}
                        </a>
                        <a href="../template_ajax/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/ajax.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../template_angularjs/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/angular1x.jpg);"></span> */}
                        </a>
                        <a href="../template_angularjs8/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/angular8x.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../template_laravel/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/laravel.jpg);"></span> */}
                        </a>
                        <a href="../template_vuejs/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/vuejs.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../template_reactjs/index_v2.html">
                            {/* <span style="background-image: url(../assets/img/version/reactjs.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="divider"></div>
                    <h5>Frontend Design (4)</h5>
                    <div className="theme-version">
                        <a href="../../../frontend/template/template_one_page_parallax/index.html">
                            {/* <span style="background-image: url(../assets/img/theme/one-page-parallax.jpg);"></span> */}
                        </a>
                        <a href="../../../frontend/template/template_e_commerce/index.html">
                            {/* <span style="background-image: url(../assets/img/theme/e-commerce.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="theme-version">
                        <a href="../../../frontend/template/template_blog/index.html">
                            {/* <span style="background-image: url(../assets/img/theme/blog.jpg);"></span> */}
                        </a>
                        <a href="../../../frontend/template/template_forum/index.html">
                            {/* <span style="background-image: url(../assets/img/theme/forum.jpg);"></span> */}
                        </a>
                    </div>
                    <div className="divider"></div>
                    <div className="row m-t-10">
                        <div className="col-md-12">
                            <a href="https://seantheme.com/color-admin/documentation/" className="btn btn-inverse btn-block btn-rounded" target="_blank"><b>Documentation</b></a>
                            <a href={void(0)} className="btn btn-default btn-block btn-rounded" data-click="reset-local-storage"><b>Reset Local Storage</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

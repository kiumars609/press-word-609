import React from 'react'

export default function WarningBox({ title = 'Warning', data }) {

    const lists = data.map((item, index) => {
        return (
            <li key={index}>{item.text}</li>
        )
    })

    return (
        <>
            <div className="note note-yellow m-b-15 m-5">
                <div className="note-icon f-s-20">
                    <i className="fa fa-lightbulb fa-2x"></i>
                </div>
                <div className="note-content">
                    <h4 className="m-t-5 m-b-5 p-b-2">{title}</h4>
                    <ul className="m-b-5 p-l-25">
                        {lists}
                    </ul>
                </div>
            </div>
        </>
    )
}

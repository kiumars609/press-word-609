import React from 'react'
import { Link } from 'react-router-dom'
import Navs from './Navs'
import './style.css'

export default function Menu() {
  return (
    <>
      <div id="sidebar" className="sidebar">
        <div data-scrollbar="true" data-height="100%">
          <ul className="nav">
            <li className="nav-profile">
              <a href={void (0)} data-toggle="nav-profile">
                <div className="cover with-shadow"></div>
                <div className="image">
                  <img src="/assets/dashboard/img/user/user-admin2.png" alt="" />
                </div>
                <div className="info">
                  <b className="caret pull-right"></b>Admin
                  <small>Manager of Web</small>
                </div>
              </a>
            </li>
            <li>
              <ul className="nav nav-profile">
                <li><Link to={'/admin-p/setting'}><i className="fa fa-cog"></i> Settings</Link></li>
                <li><a href={void (0)}><i className="fa fa-question-circle"></i> Helps</a></li>
              </ul>
            </li>
          </ul>
          <ul className="nav"><li className="nav-header">Navigation</li>
            <Navs />
          </ul>
        </div>
      </div>
      <div className="sidebar-bg"></div>
    </>
  )
}

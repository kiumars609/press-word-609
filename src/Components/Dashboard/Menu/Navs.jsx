import React from 'react'
import { Link } from 'react-router-dom'

export default function Navs() {
  return (
    <>
      <li className="active">
        <Link to={'/admin-p/dashboard'}>
          <i className="fa fa-th-large"></i>
          <span>Dashboard</span>
        </Link>
      </li>
      <li className="has-sub">
        <a href={void (0)}>
          <b className="caret"></b>
          <i className="fa fa-thumbtack"></i>
          <span>Blog</span>
        </a>
        <ul className="sub-menu">
          <li><Link to={'/admin-p/show-blog'}>All Blogs</Link></li>
          <li><Link to={'/admin-p/add-blog'}>Add Blog</Link></li>
          <li><Link to={'/admin-p/blog-category'}>Categories</Link></li>
          <li><a href="index_v2.html">Tags</a></li>
        </ul>
      </li>
    </>
  )
}

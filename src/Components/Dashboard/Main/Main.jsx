import React from 'react'

export default function Main({ children }) {
  return (
    <>
      <div id="content" className="content">
        {children}
      </div>
    </>
  )
}

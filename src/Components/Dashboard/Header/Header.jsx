import React from 'react'
import { useAuthDispatcher } from '../../../Context/auth-context';
import { actionTypes } from '../../../Context/reducer';
import './style.css'

export default function Header() {

  const dispatcher = useAuthDispatcher()

  const handleLogOut = () => {
    const token = localStorage.getItem('token')
    if (token) {
      localStorage.removeItem('token')
      dispatcher({
        type: actionTypes.LOGOUT
      })
    }
  }

  return (
    <>
      {/* <!-- begin #header --> */}
      <div id="header" className="header navbar-default">
        {/* <!-- begin navbar-header --> */}
        <div className="navbar-header">
          <a href="index.html" className="navbar-brand"><span className="navbar-logo">
            <img src='/assets/img/logo/k-logo.png' className="col-md-12" />
          </span> <b>609</b> Press</a>
          <button type="button" className="navbar-toggle" data-click="sidebar-toggled">
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
        </div>
        {/* <!-- end navbar-header --><!-- begin header-nav --> */}
        <ul className="navbar-nav navbar-right">
          <li className="navbar-form">
            <form action="" method="POST" name="search">
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter keyword" />
                <button type="submit" className="btn btn-search"><i className="fa fa-search"></i></button>
              </div>
            </form>
          </li>
          <li className="dropdown">
            <a href="#" data-toggle="dropdown" className="dropdown-toggle f-s-14">
              <i className="fa fa-bell"></i>
              <span className="label">5</span>
            </a>
            <div className="dropdown-menu media-list dropdown-menu-right">
              <div className="dropdown-header">NOTIFICATIONS (5)</div>
              <a href={void (0)} className="dropdown-item media">
                <div className="media-left">
                  <i className="fa fa-bug media-object bg-silver-darker"></i>
                </div>
                <div className="media-body">
                  <h6 className="media-heading">Server Error Reports <i className="fa fa-exclamation-circle text-danger"></i></h6>
                  <div className="text-muted f-s-10">3 minutes ago</div>
                </div>
              </a>
              <a href={void (0)} className="dropdown-item media">
                <div className="media-left">
                  <img src="/assets/dashboard/img/user/user-1.jpg" className="media-object" alt="" />
                  <i className="fab fa-facebook-messenger text-blue media-object-icon"></i>
                </div>
                <div className="media-body">
                  <h6 className="media-heading">John Smith</h6>
                  <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                  <div className="text-muted f-s-10">25 minutes ago</div>
                </div>
              </a>
              <a href={void (0)} className="dropdown-item media">
                <div className="media-left">
                  <img src="/assets/dashboard/img/user/user-2.jpg" className="media-object" alt="" />
                  <i className="fab fa-facebook-messenger text-blue media-object-icon"></i>
                </div>
                <div className="media-body">
                  <h6 className="media-heading">Olivia</h6>
                  <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                  <div className="text-muted f-s-10">35 minutes ago</div>
                </div>
              </a>
              <a href={void (0)} className="dropdown-item media">
                <div className="media-left">
                  <i className="fa fa-plus media-object bg-silver-darker"></i>
                </div>
                <div className="media-body">
                  <h6 className="media-heading"> New User Registered</h6>
                  <div className="text-muted f-s-10">1 hour ago</div>
                </div>
              </a>
              <a href={void (0)} className="dropdown-item media">
                <div className="media-left">
                  <i className="fa fa-envelope media-object bg-silver-darker"></i>
                  <i className="fab fa-google text-warning media-object-icon f-s-14"></i>
                </div>
                <div className="media-body">
                  <h6 className="media-heading"> New Email From John</h6>
                  <div className="text-muted f-s-10">2 hour ago</div>
                </div>
              </a>
              <div className="dropdown-footer text-center">
                <a href={void (0)}>View more</a>
              </div>
            </div>
          </li>
          <li className="dropdown navbar-user">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <img src="/assets/dashboard/img/user/user-13.jpg" alt="" />
              <span className="d-none d-md-inline">Adam Schwartz</span> <b className="caret"></b>
            </a>
            <div className="dropdown-menu dropdown-menu-right">
              <a href={void (0)} className="dropdown-item">Edit Profile</a>
              <a href={void (0)} className="dropdown-item"><span className="badge badge-danger pull-right">2</span> Inbox</a>
              <a href={void (0)} className="dropdown-item">Calendar</a>
              <a href={void (0)} className="dropdown-item">Setting</a>
              <div className="dropdown-divider"></div>
              <a href="" onClick={handleLogOut} className="dropdown-item">Log Out</a>
            </div>
          </li>
        </ul>
        {/* <!-- end header-nav --> */}
      </div>
      {/* <!-- end #header --> */}
    </>
  )
}

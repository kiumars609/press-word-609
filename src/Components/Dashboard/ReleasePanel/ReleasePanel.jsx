import React from 'react'
import './style.css'

export default function ReleasePanel() {
    return (
        <>
            <div className='col-md-12 main-panel-released'>
                <h5 className='header-release'>
                    Release
                </h5>
                <label className='col-md-12'>
                    Status: <span>Draft</span>
                </label>
                <label className='col-md-12'>
                    <button className='btn btn-primary col-md-12'>Release</button>
                </label>
            </div>
        </>
    )
}

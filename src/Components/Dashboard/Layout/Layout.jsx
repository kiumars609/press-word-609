import React from 'react'
import Header from '../Header'
import Menu from '../Menu'
import Main from '../Main'
import ThemePanel from '../ThemePanel'
import PageHeader from '../PageHeader/PageHeader'
import Panel from '../Panel/Panel'
import './css/default/app.min.css'
// import '/css/default/style.css'

export default function Layout({ children, title, text, firstBread, secondBread, thirdBread, fourthBread }) {
  return (
    <>
      <div id="page-container" className=" page-sidebar-fixed page-header-fixed dashboard">
        <Header />
        <Menu />
        <Main>
          <PageHeader title={title} text={text} firstBread={firstBread} secondBread={secondBread} thirdBread={thirdBread} fourthBread={fourthBread} />
          <Panel>
            {children}
          </Panel>
        </Main>
        <ThemePanel />
      </div>
    </>
  )
}

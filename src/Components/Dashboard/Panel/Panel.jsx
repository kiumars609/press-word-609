import React from 'react'

export default function Panel({ children }) {
    return (
        <div className="col-xl-12 ui-sortable">
            <div className="panel panel-inverse" data-sortable-id="table-basic-1">
                <div className="panel-heading ui-sortable-handle">
                    <h4 className="panel-title"></h4>
                    <div className="panel-heading-btn">
                        <a href={void (0)} className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                        <a href={void (0)} className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                        <a href={void (0)} className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                    </div>
                </div>
                <div className="panel-body panel-form">
                    {children}
                </div>
            </div>
        </div>
    )
}

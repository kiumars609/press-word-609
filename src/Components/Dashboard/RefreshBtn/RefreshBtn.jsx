import React from 'react'

export default function RefreshBtn({ refreshData, classBtn }) {
    return (
        <>
            <a className={`btn btn-success text-white m-2 ${classBtn}`} onClick={refreshData} data-click="panel-reload"><i className="fa fa-redo"></i> Refresh</a>
        </>
    )
}

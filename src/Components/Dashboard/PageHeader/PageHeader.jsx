import React from 'react'
import BreadCrumb from './BreadCrumb'
import Title from './Title'

export default function PageHeader({ title, text, firstBread, secondBread, thirdBread, fourthBread }) {
    return (
        <>
            {/* <!-- begin breadcrumb --> */}
            <BreadCrumb firstBread={firstBread} secondBread={secondBread} thirdBread={thirdBread} fourthBread={fourthBread} />
            {/* <!-- end breadcrumb -->
			    <!-- begin page-header --> */}
            <Title title={title} text={text} />
            {/* <!-- end page-header --> */}
        </>
    )
}

import React from 'react'

export default function BreadCrumb({ firstBread = null, secondBread = null, thirdBread = null, fourthBread = null }) {
    return (
        <ol className="breadcrumb float-xl-right">
            <li className="breadcrumb-item"><a href={void (0)}>Home</a></li>
            {null !== firstBread ? <li className="breadcrumb-item"><a href={void (0)}>{firstBread}</a></li> : ''}
            {null !== secondBread ? <li className="breadcrumb-item"><a href={void (0)}>{secondBread}</a></li> : ''}
            {null !== thirdBread ? <li className="breadcrumb-item"><a href={void (0)}>{thirdBread}</a></li> : ''}
            {null !== fourthBread ? <li className="breadcrumb-item"><a href={void (0)}>{fourthBread}</a></li> : ''}
        </ol>
    )
}

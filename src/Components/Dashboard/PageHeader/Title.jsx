import React from 'react'

export default function Title({ title = 'Default Title', text = null }) {
    return (
        <>
            {null !== title ?
                <h1 className="page-header">{title}
                    {null !== text ? <small>{text}</small> : ''}
                </h1> : ''
            }
        </>
    )
}

// import logo from './logo.svg';
import './App.css';
import { useAuthState } from './Context/auth-context';
import Login from './Page/Login';
import Dashboard from './Page/Dashboard'
import { Route } from 'react-router';
import { Routes } from 'react-router-dom';
import AddBlog from './Page/Dashboard/Blog/AddBlog';
import Blog from './Page/Dashboard/Blog/Blog';
import BlogCategory from './Page/Dashboard/Blog/Category/BlogCategory';
import Setting from './Page/Dashboard/Setting';

function App() {
  const { token } = useAuthState()
  return (
    <>
      {token ?
        <Routes>
          {/* Dashboard */}
          <Route path='admin-p/dashboard' element={<Dashboard />} />
          <Route path='admin-p/add-blog' element={<AddBlog />} />
          <Route path='admin-p/show-blog' element={<Blog />} />
          <Route path='admin-p/blog-category' element={<BlogCategory />} />
          <Route path='admin-p/setting' element={<Setting />} />
        </Routes>
        :
        <Login />}
    </>
  )
}

export default App;

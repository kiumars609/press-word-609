import React, { useState, useEffect } from 'react'
import { get } from '../../../../services/HttpClient'
import Loading from '../../../../Components/Dashboard/Loading/Loading'
import DataTable from '../../../../Components/Dashboard/DataTable/DataTable'

export default function ShowBlogCategory() {
    const [data, setData] = useState([])
    const [refreshPanel, setRefreshPanel] = useState(0)
    const [loader, setLoader] = useState(true)

    useEffect(() => {
        get('/blog-category')
            .then(response => {
                setData(response)
                setLoader(false)
            })
    }, [refreshPanel]);

    // When use Table Component you must set thead for table
    const dataTitles = [
        { title: 'Id', field: 'id' },
        { title: 'Title', field: 'title' },
        { title: 'Description', field: 'description' },
        { title: 'Slug', field: 'slug' },
    ]

    return (
        <>
            <div className="col-md-7">
                {loader ? <Loading /> :
                    <div className='mt-2 row mx-auto'>
                        <div className="col-md-6 pr-0 d-flex align-items-end justify-content-end"><span className='fw-bold float-end'>{data.length} Item</span></div>
                        <DataTable
                            refreshData={() => setRefreshPanel(key => key + 1)}
                            filtring={true}
                            title={dataTitles}
                            data={data}
                            deleteAction={true}
                            deleteRoute={'blog-category'}
                        />
                    </div>
                }
            </div>
        </>
    )
}

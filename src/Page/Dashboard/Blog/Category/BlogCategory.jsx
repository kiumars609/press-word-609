import React from 'react'
import Layout from '../../../../Components/Dashboard/Layout'
import Tostify from '../../../../Components/Dashboard/Tostify'
import AddBlogCategory from './AddBlogCategory'
import ShowBlogCategory from './ShowBlogCategory'

export default function BlogCategory() {
  return (
    <>
      {/* Layout should use in all pages */}
      <Layout title={'Category'} firstBread={'Blog'} secondBread={'Category'}>
        {/* Use toastify with toast function for set texts and type for Exp.(success, error, ...) */}
        <Tostify />
        <div className="row mx-auto p-5">
          {/* All form for post data */}
          <AddBlogCategory />
          {/* All table data */}
          <ShowBlogCategory />
        </div>
      </Layout>
    </>
  )
}

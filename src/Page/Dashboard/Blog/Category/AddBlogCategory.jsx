import React, { useState } from 'react'
import { post } from '../../../../services/HttpClient'
import { toast } from 'react-toastify'
import useFormFields from '../../../../useFormFields'

const postData = async (title, slug, description) => {
    return post('/blog-category', {
        title: title,
        slug: slug,
        description: description
    }).then(response => response.data)
}
export default function AddBlogCategory() {
    const [errorBorder, setErrorBorder] = useState('')
    // This function for set value and onchange in all input in page
    const { fields, setFields, handleFields } = useFormFields({
        title: '',
        slug: '',
        description: '',
    })

    // Function for onSubmit in form tag
    const handleSubmit = (e) => {
        e.preventDefault();
        // Show error when input is empty
        if (fields.title === undefined) {
            toast['error']('Please Complete All Required Fields')
            setErrorBorder('border border-danger')
        }
        // Show success when input not empty
        else {
            postData(fields.title, fields.slug || 'Uncategorized', fields.description || '-')
                // Show toast function when data post successfully
                .then(toast['success']('Blog Inserted Successfully !!'))
            setFields('')
            setErrorBorder('')
        }
    }

    return (
        <>
            <div className="col-md-5">
                <h4>Add New Category</h4>
                <form onSubmit={handleSubmit}>
                    <label className='col'>
                        <span className='fw-bold'>Title</span>
                        <input type='text' className={`form-control ${errorBorder}`} name='title' onChange={handleFields} value={fields.title || ''} />
                        <span>Is displayed on the site with this title.</span>
                    </label>
                    <label className='col'>
                        <span className='fw-bold'>Slug</span>
                        <input type='text' className='form-control' name='slug' onChange={handleFields} value={fields.slug || ''} />
                        <span>The name is the Latin version of the word used in URLs.. Use only letters, numbers and dashes for naming. The display will be in lower case only.</span>
                    </label>
                    <label className='col'>
                        <span className='fw-bold'>Description</span>
                        <textarea className='form-control' name='description' onChange={handleFields} value={fields.description || ''}></textarea>
                        <span>Descriptions are not important by default, however some skins may display them.</span>
                    </label>
                    <label className='col'>
                        <button className='btn btn-primary mt-5'>Add New Category</button>
                    </label>
                </form>
            </div>
        </>
    )
}

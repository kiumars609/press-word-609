import React, { useState } from 'react'
import Layout from '../../../Components/Dashboard/Layout'
import ReleasePanel from '../../../Components/Dashboard/ReleasePanel/ReleasePanel'
import { post } from '../../../services/HttpClient'
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Tostify from '../../../Components/Dashboard/Tostify'
import useFormFields from '../../../useFormFields';
import { toast } from 'react-toastify';

const postData = async (title, text) => {
  return post('/blog', {
    title: title,
    text: text
  }).then(response => response.data)
}

export default function AddBlog() {
  const [text, setText] = useState('')
  const { fields, setFields, handleFields } = useFormFields({ title: '' })
  const handleSubmit = (e) => {
    e.preventDefault()
    postData(fields.title, text)
      .then(toast['success']('Blog Inserted Successfully !!'))
    setFields('')
    setText('')
  }

  return (
    <Layout title={'Add Blog'} firstBread={'Blog'} secondBread={'Add Blog'}>
      <Tostify />
      <form onSubmit={handleSubmit}>
        <div className="row mx-auto">
          <div className="col-md-3 p-0">
            <ReleasePanel />
          </div>
          <div className="col-md-9">
            <div className="col-md-12 my-2 col-sm-8">
              <input className="form-control" type="text" name="title" placeholder="Title" data-parsley-required="true" value={fields.title || ''} onChange={handleFields} />
            </div>
            <div className="col-md-12 my-2 col-sm-8">
              <CKEditor
                editor={ClassicEditor}
                config={{
                  showPreviews: true,
                }}
                // disabled={true}
                data={text}
                placeholder='asd'
                onChange={(event, editor) => {
                  const data = editor.getData();
                  setText(data)
                }}
              />
            </div>
          </div>
        </div>
      </form>
    </Layout>
  )
}

import React, { useState, useEffect } from 'react'
import DataTable from '../../../Components/Dashboard/DataTable/DataTable';
import Layout from '../../../Components/Dashboard/Layout'
import Loading from '../../../Components/Dashboard/Loading/Loading';
import { get } from '../../../services/HttpClient';

export default function Blog() {
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(true)
  const [refreshPanel, setRefreshPanel] = useState(0)
  useEffect(() => {
    get('/blog')
      .then(response => {
        setData(response);
        setLoader(false)
      })
  }, [refreshPanel]);

  const dataTitles = [
    { title: 'ID', field: 'id' },
    { title: 'TITLE', field: 'title' },
    { title: 'TEXT', field: 'text' }
  ]

  return (
    <>
      <Layout title={'All Blogs'} firstBread={'Blog'} secondBread={'All Blogs'}>
        {loader ? <Loading /> :
          <DataTable refreshData={() => setRefreshPanel(key => key + 1)} filtring={true} title={dataTitles} data={data} />
        }
      </Layout>
    </>
  )
}

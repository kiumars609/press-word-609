import React from 'react'
import Layout from '../../../Components/Dashboard/Layout'
import FavIcon from './FavIcon/FavIcon'

export default function Setting() {
  return (
    <>
      <Layout title={'Setting'} firstBread={'Setting'}>
        <div className="row p-5 mx-auto">
          {/* Upload FavIcon */}
          <div className="col-md-6 border rounded">
            <label>
              <span className='fw-bold fs-5 pl-1 text-Secondary'>Fav Icon</span>
            <FavIcon />
            </label>
          </div>
        </div>
      </Layout>
    </>
  )
}

import React, { useEffect, useState } from 'react'
import './style.css'
import WarningBox from '../../../../Components/Dashboard/WarningBox/WarningBox'
import { toast } from 'react-toastify'
import Tostify from '../../../../Components/Dashboard/Tostify'
import { del, get, post } from '../../../../services/HttpClient'

export default function FavIcon() {
    const [files, setFiles] = useState(null)
    const [loader, setLoader] = useState(0)
    const [data, setData] = useState(null)

    const warnings = [
        { text: 'The maximum file size for uploads in this demo is 5 MB (default file size is unlimited).' },
        { text: 'Uploaded files will be deleted automatically after 5 minutes (demo setting).' },
    ]

    const handleFile = (e) => {
        setFiles(e.target.files[0])
    }

    const handleUpload = (e) => {
        e.preventDefault()
        if (files !== null) {
            const reader = new FileReader();
            reader.readAsDataURL(files)
            reader.onload = (e) => {
                setLoader(true);
                files !== null ?
                    data == null ?
                        post('/fav-icon', {
                            icon: reader.result,
                        }).then(response => {
                            toast['success']('FavIcon Uploaded!');
                            setLoader(false);
                        })
                        :
                        del(`/fav-icon/1`)
                            .then(
                                post('/fav-icon', {
                                    icon: reader.result,
                                }).then(response => {
                                    toast['success']('FavIcon Uploaded!');
                                    setLoader(false);
                                })
                            )
                    : toast['error']('Please Insert an Image');
            }
        }
    }

    useEffect(() => {
        get('/fav-icon')
            .then(response => {
                setData(response[0].icon)
            })
    }, [handleUpload]);

    return (
        <>
            <WarningBox data={warnings} />
            <Tostify />
            <form>
                <div className="col-md-12 row mx-auto p-5">
                    <span className="btn btn-primary fileinput-button m-2 col-md-4">
                        <i className="fa fa-fw fa-plus"></i>
                        <span>Add Icon</span>
                        <input type="file" name="" onChange={(e) => handleFile(e)} />
                    </span>
                    <button className="btn btn-primary text-white m-2 col-md-4" onClick={(e) => handleUpload(e)}>
                        {!loader ? <i className="fa fa-upload"></i> : <img src='/assets/img/loading/lod.gif' className='uploader-icon' />} Start Upload
                    </button>
                </div>
            </form>
            <div className="col-md-12 back-show-image p-5">
                {null == data ?
                    <div className="col-md-6 text-center main-no-image">
                        <i className="fa fa-file fa-3x"></i>
                        <br />
                        <span>No file selected</span>
                    </div>
                    :
                    <div className="col-md-6 text-center main-no-image">
                        <img src={data} className='col-md-6' />
                    </div>
                }
            </div>
        </>
    )
}

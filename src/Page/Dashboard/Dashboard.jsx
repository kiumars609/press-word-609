import React from 'react'
import Layout from '../../Components/Dashboard/Layout/'

export default function Dashboard() {
  return (
    <>
      <Layout title={'Dashboard'} text={'Welcome to Wordpress'} firstBread={'Dashboard'}>
        {/*<!-- begin row --> */}
        <div className="row">
          {/* <!-- begin col-3 --> */}
          <div className="col-xl-3 col-md-6">
            <div className="widget widget-stats bg-teal">
              <div className="stats-icon stats-icon-lg"><i className="fa fa-globe fa-fw"></i></div>
              <div className="stats-content">
                <div className="stats-title">TODAY'S VISITS</div>
                <div className="stats-number">7,842,900</div>
                <div className="stats-progress progress">
                  <div className="progress-bar" style={{ width: '70.1%' }}></div>
                </div>
                <div className="stats-desc">Better than last week (70.1%)</div>
              </div>
            </div>
          </div>
          {/* <!-- end col-3 -->
				    <!-- begin col-3 --> */}
          <div className="col-xl-3 col-md-6">
            <div className="widget widget-stats bg-blue">
              <div className="stats-icon stats-icon-lg"><i className="fa fa-dollar-sign fa-fw"></i></div>
              <div className="stats-content">
                <div className="stats-title">TODAY'S PROFIT</div>
                <div className="stats-number">180,200</div>
                <div className="stats-progress progress">
                  <div className="progress-bar" style={{ width: '40.5%' }}></div>
                </div>
                <div className="stats-desc">Better than last week (40.5%)</div>
              </div>
            </div>
          </div>
          {/* <!-- end col-3 -->
				    <!-- begin col-3 --> */}
          <div className="col-xl-3 col-md-6">
            <div className="widget widget-stats bg-indigo">
              <div className="stats-icon stats-icon-lg"><i className="fa fa-archive fa-fw"></i></div>
              <div className="stats-content">
                <div className="stats-title">NEW ORDERS</div>
                <div className="stats-number">38,900</div>
                <div className="stats-progress progress">
                  <div className="progress-bar" style={{ width: '76.3%' }}></div>
                </div>
                <div className="stats-desc">Better than last week (76.3%)</div>
              </div>
            </div>
          </div>
          {/* <!-- end col-3 --> */}
          {/* <!-- begin col-3 --> */}
          <div className="col-xl-3 col-md-6">
            <div className="widget widget-stats bg-dark">
              <div className="stats-icon stats-icon-lg"><i className="fa fa-comment-alt fa-fw"></i></div>
              <div className="stats-content">
                <div className="stats-title">NEW COMMENTS</div>
                <div className="stats-number">3,988</div>
                <div className="stats-progress progress">
                  <div className="progress-bar" style={{ width: '54.9%' }}></div>
                </div>
                <div className="stats-desc">Better than last week (54.9%)</div>
              </div>
            </div>
          </div>
          {/* <!-- end col-3 --> */}
        </div>
        {/* <!-- end row --> */}
      </Layout>
    </>
  )
}

import React, { useState, useEffect, useLayoutEffect } from 'react'
import './style.css'
import axios from 'axios'
import { useAuthDispatcher, useAuthState } from '../../Context/auth-context'
import { actionTypes } from '../../Context/reducer'

const fetchToken = async (username, password) => {
  return axios.post('http://localhost:3001/login', {
    username,
    password
  }).then(response => response.data)
}

const fetchUserInfo = (token) => {
  return axios.get('http://localhost:3001/users/me', {
    headers: {
      authorization: token
    }
  }).then(response => response.data)
}

export default function Login() {
  const [user, setUser] = useState('')
  const [pass, setPass] = useState('')
  const [token, setToken] = useState(null)
  const dispatcher = useAuthDispatcher()
  const { loading } = useAuthState()

  useLayoutEffect(() => {
    const token = localStorage.getItem('token')
    if (token) {
      setToken(token)
      dispatcher({
        type: actionTypes.LOGIN_REQUEST
      })
    }
  },[dispatcher])

  useEffect(() => {
    if (token) {
      fetchUserInfo(token)
        .then(({ success, data }) => {
          if (success) {
            localStorage.setItem('token', token)
            dispatcher({
              type: actionTypes.LOGIN_SUCCESS,
              payload: {
                user: data,
                token
              }
            })
          }
        })
    }
  }, [token, dispatcher]);

  const handleSubmit = (e) => {
    e.preventDefault()
    dispatcher({
      type: actionTypes.LOGIN_REQUEST
    })
    fetchToken(user, pass)
      .then(({ success, data }) => {
        if (success) {
          setToken(data)
        }
      })
  }

  return (
    <>
      {loading ? <p>Loading</p> : <div className='main row mx-auto'>
        <div className="col-md-5 main-panel">
          <div className='col-md-8 login-panel'>
            <img src='assets/img/logo/k-logo.png' className='col-md-2' alt='logo' />
            <form onSubmit={handleSubmit}>
              <input type='text' className='col-md-12' placeholder='EMAIL*' value={user} onChange={(e) => setUser(e.target.value)} />
              <input type='password' className='col-md-12' placeholder='PASSWORD*' value={pass} onChange={(e) => setPass(e.target.value)} />

              {/* <label>
                    <input type="radio" />
                    <span className='text-white'>REMEMBER ME?</span>
                </label> */}

              <button className='btn btn-success'>LogIn</button>
            </form>
          </div>
        </div>
      </div>}
    </>
  )
}
